#!/bin/bash
DATETIMESTAMP=`date '+%m-%d-%Y %H:%M:%S'`

RUNTESTMODE=$1
CONFIGNAME=$2
ENV=$3
REVMODE=$4
GENCOMBINEDCTL=$5

doerror()
{
        echo "Usage $0 <Test mode (Y/N)> <config name> <env: (test/prod)> <rev mode: (FULL/INCREMENTAL)> <combine into one CTL file: (Y/N)>"
        exit 1
}

if [ "${RUNTESTMODE}" == "Y" ]; then
        export LIMITROWS="100"
        export SPLITSIZE="30"
elif [ "${RUNTESTMODE}" == "N" ]; then
        export SPLITSIZE=5000000
else
	doerror
fi
[ -z "${DBEXPORTHOME}" ] && echo "DBEXPORTHOME is not defined." && exit 1
[ -z "${CONFIGNAME}" ] && doerror
[ "${ENV}" != "test" ] && [ "${ENV}" != "prod" ] && doerror
[ "${REVMODE}" != "FULL" ] && [ "${REVMODE}" != "INCREMENTAL" ] && doerror
[ "${GENCOMBINEDCTL}" != "Y" ] && [ "${GENCOMBINEDCTL}" != "N" ] && doerror

${DBEXPORTHOME}/deployment/extractDBUsingConfig.sh ${CONFIGNAME} ${DBEXPORTHOME}/batch_control_data/${ENV} ${GENCOMBINEDCTL}  N ${REVMODE}
