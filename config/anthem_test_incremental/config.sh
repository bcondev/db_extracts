#!/bin/bash
DBNAME=(ag_test)
VAM=(TEST)
LOGIN=(local)
FPREFIX=D_BEACON_

# This will be used to use to generate the NAME instead of DESCRIPTION for code tables - use in codeFileExport.sh
export USENAMECODEFILE="|CASE_DISPOSITION|"
