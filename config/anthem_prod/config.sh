#!/bin/bash
DBNAME=(ag_prod_new)
VAM=(PROD)
FPREFIX=D_BEACON_
#This is for reading from master
LOGIN=(local)
#This is for reading from VAM 5
#LOGIN=(vam5)

# This will be used to use to generate the NAME instead of DESCRIPTION for code tables - use in codeFileExport.sh
export USENAMECODEFILE="|CASE_DISPOSITION|"
