#!/bin/bash
export CONFIGNAME=$1
export BATCHCONTROLDIR=$2
export GENCOMBINEDCTL=$3
export TESTMODE=$4
export REVISIONMODE=$5

#this if we should limit the number of rows per pull - for testing only
#export LIMITROWS=100
#this if we should split
#export SPLITSIZE=30

export SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

doerror () {
	# param 1 is rc, 2 is error message
	echo "Usage: $0 <config name> <batch control dir> <Create consolidated CTL (Y/N)> [Mode (Y/N/FULL/INCREMENTAL)]"
	echo "${2}"
	exit ${1}
}

cleanOnExit () { 
	rmdir $LOCKNAME 
}

get_batch_number()
{
	BATCHNUMBERFILE=$1
	MODE=$2
	# handle control file batch number
	# if we don't have a file, start at zero
	if [ ! -f "${BATCHNUMBERFILE}" ] ; then
	  BATCHNUMBER=0

	# otherwise read the batchNumber from the file
	else
	  BATCHNUMBER=`cat ${BATCHNUMBERFILE}`
	fi

	# increment the batchNumber
	BATCHNUMBER=`expr ${BATCHNUMBER} + 1`
if [ "${MODE}" == "N" ]; then echo "${BATCHNUMBER}" > ${BATCHNUMBERFILE}
	fi
}


[ -z "${CONFIGNAME}" ] && doerror 1 "Config name not provided"
[ -z "${BATCHCONTROLDIR}" ] && doerror 1 "Batch control directory not provided"
[ -z "${GENCOMBINEDCTL}" ] && doerror 1 "Generate combined CTL was not provided"
[ -z "${TESTMODE}" ] && doerror 1 "Test mode required (Y/N)"

. ${SCRIPT_DIR}/scripts/env.sh 
. ${SCRIPT_DIR}/scripts/common.sh 

cd ${SCRIPT_DIR}

export CONFIGDIR=${EXPORTHOME}/deployment/config/${CONFIGNAME}
[ ! -d "${CONFIGDIR}" ] && doerror 1 "Config dir: ${CONFIGDIR} is not valid"
[ ! -d "${EXPORTLOG}" ] && doerror 1 "Log dir: ${EXPORTLOG} is not valid"
[ ! -d "${EXPORTHOME}" ] && doerror 1 "Export home dir: ${EXPORTHOME} is not valid"
[ ! -d "${BATCHCONTROLDIR}" ] && doerror 1 "Export batch control dir: ${BATCHCONTROLDIR} is not valid"
[ ! -f "${BATCHCONTROLDIR}/codeFile_batchNumber.dat" ] && doerror 1 "CodeFile batch control file: ${BATCHCONTROLDIR}/codeFile_batchNumber.dat is not valid"
[ ! -f "${BATCHCONTROLDIR}/dataFile_batchNumber.dat" ] && doerror 1 "DataFile batch control file: ${BATCHCONTROLDIR}/dataFile_batchNumber.dat is not valid"
#FILESCHECK=`ls ${EXPORTOUTPUT} | wc -l` 
#[ $FILESCHECK -gt 0 ] && doerror 1 "WIP dir: ${EXPORTOUTPUT} is not empty."
[ ! -d "${EXPORTOUTPUT}" ] && mkdir "${EXPORTOUTPUT}"

LOCKNAME=/tmp/${CONFIGNAME}.lock
mkdir "$LOCKNAME"
if [ $? -ne 0 ]
then
        exit 0
fi

# CLeanup just in case they press CTRL-C or kill it
trap cleanOnExit 0 1 2 15

DATETIMESTAMP=`date '+%m-%d-%Y %H:%M:%S'`
echo "Start : "${DATETIMESTAMP} >> ${EXPORTLOG}/dbExport.log
echo "----------------------------------------------------------" >> ${EXPORTLOG}/dbExport.log

INCREMENTALTABLESFILE=${CONFIGDIR}/incrementalTables.txt
export LOWERCASEVAM=$(echo ${VAM[0]} | sed 's/./\L&/g')
#Are we in incremental mode?  Make sure we have baseline data
if [ "${REVISIONMODE}" == "INCREMENTAL" ]; then
	#When running in incremental node, we need to have a baseline directory 
	[ ! -d "${BASELINEDIR}" ]  && doerror 1 "Baseline dir needed:  ${BASELINEDIR}"
	[ ! -f "${INCREMENTALTABLESFILE}" ]  && doerror 1 "Incremental config file needed:  ${INCREMENTALTABLESFILE}"

	#Validate that all of the files are present in the baseline for us to diff
	while IFS= read -r t; do
		[ ! -f `ls ${BASELINEDIR}/current/*vam_${LOWERCASEVAM}_${t}_2*.txt` ] && doerror 1 "No baseline file exists for table: $t in ${BASELINEDIR}/current.  Did you mean to run in FULL mode?"
	done < "${INCREMENTALTABLESFILE}"
fi


get_batch_number ${BATCHCONTROLDIR}/dataFile_batchNumber.dat ${MODE}
echo "Incrementing batch number : ${BATCHNUMBER}"

#This is to allow the children shells to see if we need to create the "ALL" ctl files.  Currently only Anthem asks for this.
if [ -f ${CONFIGDIR}/codeFileTables.txt ] && [ -f ${CONFIGDIR}/codeFileTablesNoStartDate.txt ]; then
	${EXPORTHOME}/deployment/scripts/codeFileExport.sh ${CONFIGDIR}/config.sh ${BATCHCONTROLDIR}/codeFile_batchNumber.dat ${CONFIGDIR}/codeFileTables.txt ${CONFIGDIR}/codeFileTablesNoStartDate.txt ${EXPORTOUTPUT} ${TESTMODE} >> ${EXPORTLOG}/codeFileExport.log 2>&1
fi

if [ -f ${CONFIGDIR}/exportSQL.txt ]; then
	${EXPORTHOME}/deployment/scripts/dbExport.sh ${CONFIGDIR}/config.sh ${BATCHNUMBER} ${CONFIGDIR}/exportSQL.txt ${EXPORTOUTPUT} ${TESTMODE} >> ${EXPORTLOG}/dbExport.log 2>&1
fi

#The files listed in this should NOT contain any updates
if [ -f ${CONFIGDIR}/incrementalExportSQLNoUpdates.txt ]; then
	#Reset of the incremental data to only have the first row (which will result in pulling all of the data)
	if [ "${REVISIONMODE}" == "INCREMENTAL" ] && [ ! -z "${RESETBASELINE}" ]; then
		for i in ${BATCHCONTROLDIR}/*incremental.dat; do
			echo "Resetting incremental baseline file: ${i}" >> ${EXPORTLOG}/dbExport.log
			head -n 1 ${i} > ${i}.tmp && mv ${i}.tmp ${i}
		done
	fi
	${EXPORTHOME}/deployment/scripts/dbExport_incremental_no_updates.sh ${CONFIGDIR}/config.sh ${BATCHNUMBER} ${CONFIGDIR}/incrementalExportSQLNoUpdates.txt ${EXPORTOUTPUT} ${BATCHCONTROLDIR} ${TESTMODE} >> ${EXPORTLOG}/dbExport.log 2>&1
fi

if [ -f ${CONFIGDIR}/incrementalTables.txt ] && ( [ "${REVISIONMODE}" == "FULL" ] || [ "${REVISIONMODE}" == "INCREMENTAL" ] ); then
	${EXPORTHOME}/deployment/scripts/dbExport_incremental.sh ${CONFIGDIR}/config.sh ${BATCHNUMBER} ${CONFIGDIR}/exportSQL.txt ${INCREMENTALTABLESFILE} ${EXPORTOUTPUT} ${TESTMODE} ${REVISIONMODE}  >> ${EXPORTLOG}/dbExport.log 2>&1
fi

#Split the files
if [ ! -z "${SPLITSIZE}" ] && [ -f ${CONFIGDIR}/incrementalTables.txt ]; then
	cd ${EXPORTOUTPUT}
	
	#Split on the INCREMENTAL files
	while IFS= read -r t; do
		CURRENTFILE=`ls ${FDIR}vam_${LOWERCASEVAM}_${t}_2*.txt`
		CURRENTINCREMENTALFILE=`ls ${FDIR}vam_${LOWERCASEVAM}_${t}_INCREMENTAL_2*.txt 2> /dev/null`
		SPLITFILE=$CURRENTINCREMENTALFILE
		[ ! -f "${SPLITFILE}" ] && SPLITFILE=${CURRENTFILE}
		[ ! -f "${SPLITFILE}" ] && doerror 1 "The output file: ${SPLITFILE} is missing during split detection." "${RECIPIENTS}"
		echo "Looking for file ${CURRENTFILE} to split" >> ${EXPORTLOG}/dbExport.log
		ALLCOUNT=$(cat ${SPLITFILE} | wc -l)
		if [ $ALLCOUNT -gt $SPLITSIZE ]; then
			FILENOEXT="${SPLITFILE%.txt}"
			header=$(head -n 1 "$SPLITFILE")
			if [ "${REVISIONMODE}" == "FULL" ]; then
				tail -n +2 ${SPLITFILE} | sed 's/$/|/' |  split -l ${SPLITSIZE} - "${FILENOEXT}_" -d -a 3 
			else
				tail -n +2 ${SPLITFILE} |  split -l ${SPLITSIZE} - "${FILENOEXT}_" -d -a 3 
			fi
			for f in ${FILENOEXT}_*
			do
			    (echo "$header" && cat "$f") > "${f}.txt"
			    rm "${f}"
			done
		fi
	done < "${INCREMENTALTABLESFILE}"

	for i in *_000.txt; do
		NEWFILENAME=$(echo "$i"  | sed 's/_000.txt/.txt/')
		mv $i $NEWFILENAME
	done
	cd -
fi

#Generate the control files
make_control_file  "${EXPORTOUTPUT}"

# Create done file, needs to be made after all tables dumped
for i in ${!DONEFILENAMES[@]};do
touch ${FDIR}${DONEFILENAMES[$i]}_${DATESTAMP}_${TIMESTAMP}.done
done


#Do some cleanup - remove temp files
rm ${EXPORTOUTPUT}/*.tmp > /dev/null 2>&1


chown -R sftpaccess:tomcat ${EXPORTOUTPUT}
mv ${EXPORTOUTPUT}/* ${FINALEXPORTOUTPUT}

echo "----------------------------------------------------------" >> ${EXPORTLOG}/dbExport.log
echo "Finish: " `date '+%m-%d-%Y %H:%M:%S'` >> ${EXPORTLOG}/dbExport.log
echo "----------------------------------------------------------" >> ${EXPORTLOG}/dbExport.log



