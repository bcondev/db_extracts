#/bin/bash
ENV=$1
REVMODE=$2

doerrorargs()
{
        echo "Usage $0 <env: (test/prod)> <rev mode: (FULL/INCREMENTAL)>"
        exit 1
}
doerror()
{
	errMsg=$1
        echo "${errMsg}"
        exit 1
}
[ -z "${DBEXPORTHOME}" ] && echo "DBEXPORTHOME is not defined." && exit 1
[ "${REVMODE}" != "FULL" ] && [ "${REVMODE}" != "INCREMENTAL" ] && doerrorargs
[ "${ENV}" != "test" ] && [ "${ENV}" != "prod" ] && doerrorargs
[ ! -d ${DBEXPORTHOME}/baseline ] && doerror "MIssing baseline dir: ${DBEXPORTHOME}/baseline"
[ ! -d ${DBEXPORTHOME}/files ] && doerror "MIssing files dir: ${DBEXPORTHOME}/files"
rm -rf ${DBEXPORTHOME}/baseline/D*
rm ${DBEXPORTHOME}/baseline/current
if [ "${REVMODE}" == "FULL" ]; then
	ln -s ${DBEXPORTHOME}/baseline/fullset.fortesting ${DBEXPORTHOME}/baseline/current
else
	ln -s ${DBEXPORTHOME}/baseline/incrementalset.fortesting ${DBEXPORTHOME}/baseline/current
fi
cp ${DBEXPORTHOME}/baseline/current/*.dat ${DBEXPORTHOME}/batch_control_data/${ENV}/
rm -rf ${DBEXPORTHOME}/files/D_*
rm -rf ${DBEXPORTHOME}/files/wip/*
