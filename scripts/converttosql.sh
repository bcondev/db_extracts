#!/bin/bash
PREFIX=$1
OUTPUT=$2
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
FILENAME=`basename "$0"`

if [ -z "${PREFIX}" ] || [ -z "${OUTPUT}" ]; then
        echo "Provide the fix like 'D_BEACON_vam_prod_' as a argument: ./${FILENAME} D_BEACON_vam_prod_ exportSQL.txt"
        exit 1
fi

if [ ! -f "${OUTPUT}.1" ]; then
        for i in `ls ${PREFIX}*.txt`
        do
                TABLENAME=`echo $i | awk -F "${PREFIX}" '{print $2;}' | awk -F "_2" '{print $1;}'`
                FIRSTROW=`sed '1,1!d' $i | sed 's/|/,/g'`

                echo "${TABLENAME}~select ${FIRSTROW} from ${TABLENAME}" >> ${OUTPUT}.1
        done
fi

cat ${OUTPUT}.1 | sed 's/VAM_ID/XXX/g' | sed 's/ ID,/ ID AS VAM_ID,/g' | sed 's/XXX/ID AS VAM_ID/g' | sed 's/VAM_LANGUAGE/LANGUAGE AS VAM_LANGUAGE/g'  | sed 's/VAM_USER/USER AS VAM_USER/g'  > ${OUTPUT}

