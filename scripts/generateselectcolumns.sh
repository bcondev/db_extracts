#!/bin/bash
USERID=$1
PASSWORD=$2
DBNAME=$3
TABLENAME=$4
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
FILENAME=`basename "$0"`

if [ -z "${USERID}" ] || [ -z "${PASSWORD}" ] || [ -z "${DBNAME}" ] || [ -z "${TABLENAME}" ]; then
	echo "Usage: $0 <user id> <password> <schema/db name> <table name> (i.e.  $0 root password ag_prod activity)"
        exit 1
fi

TABLENAME=`echo ${TABLENAME} | tr '[:lower:]' '[:upper:]'`
mysql --ssl-mode=DISABLED -u ${USERID} -p"${PASSWORD}" ${DBNAME} --column-names -B -e "select group_concat(column_name) from information_schema.columns  where table_schema = '${DBNAME}' and table_name = '${TABLENAME}'" |  sed 's/|/ /g'  | sed 's/\\t//g' | sed 's/\\n//g' | sed 's/\t/,/g' |  sed 's/ LANGUAGE,/LANGUAGE AS VAM_LANGUAGE,/g'  | sed 's/USER,/USER AS VAM_USER,/g' > out.txt
echo "${TABLENAME}~SELECT "`sed '2,1!d' out.txt`" FROM ${TABLENAME}" | sed 's/ ID,/ ID AS VAM_ID,/g'
rm out.txt
