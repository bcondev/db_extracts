#!/bin/bash

. $EXPORTHOME/deployment/scripts/common.sh

CONFIGFILE=$1
BATCHNUMBER=$2
EXPORTSQLFILE=$3
OUTPUTDIR=$4
TESTMODE=$5

if [ -z "${CONFIGFILE}" ] || [ -z "${EXPORTSQLFILE}" ] || [ -z "${OUTPUTDIR}" ] || [ -z "${BATCHNUMBER}" ] || [ -z "${TESTMODE}" ]; then
	echo "Usage: $0 <config file> <batch number> <export SQL file> <output dir> <test mode: Y/N>"
	doerror 1 "(i.e.  $0 ../config/ibxcom_test/config.sh 456 ../config/ibxcom_test/exportSQL.txt ../config/ibxcom_test/incrementaltables.txt files N"
	exit 1
fi
if [ -z "${TESTMODE}" ]; then
	TESTMODE=Y
fi

. $CONFIGFILE

SCRIPTNAME=`basename "$0"`

if [ "${TESTMODE}" == "N" ]; then
	echo "Running in production mode..."
	echo 'Writing Files to :' ${OUTPUTDIR}
else
	echo 'Running in TEST mode... no files will be output.  Use ./'${SCRIPTNAME}' N to produce real files.'
	mkdir __deleteme__
	FDIR=$PWD/__deleteme__/${FPREFIX}
fi

for i in ${!DBNAME[@]};
do

#dump sql tables
while read  t; do
	TABLENAME=`echo $t | cut -d'~' -f1`
	QUERYSQL=`echo $t | cut -d'~' -f2`
	if [ -n "${TABLENAME}" ] && [ -n "${QUERYSQL}" ]; then
		dump_table_using_sql "${TABLENAME}" "${QUERYSQL}" "${TESTMODE}"
	fi
done < ${EXPORTSQLFILE}


done
if [ -d "./__deleteme__" ]; then
	rm -rf __deleteme__
fi
