#!/bin/bash

. $EXPORTHOME/deployment/scripts/common.sh

#Keep 15 days of baselines
BASELINEKEPTDAYS=30
#This is the max number of records per file
CONFIGFILE=$1
BATCHNUMBER=$2
EXPORTSQLFILE=$3
INCREMENTALTABLESFILE=$4
OUTPUTDIR=$5
TESTMODE=$6
REVISIONMODE=$7
RECIPIENTS=lnguyen@beaconhcs.com


baselinedata()
{
	#Remove old baselines
	find ${BASELINEDIR} -type d -name "DBEXTRACTS_BASELINE_*" -exec rm -r {} +
	
	NEWBASELINEDIR="DBEXTRACTS_BASELINE_`date +'%Y%m%d_%H%M%S'`"
	mkdir -p ${BASELINEDIR}/$NEWBASELINEDIR
	while IFS= read -r t; do
	    CURRENTFILE=$(ls ${FDIR}vam_${LOWERCASEVAM}_${t}_2*.txt.baseline)
	    echo baselinedata current file: ${CURRENTFILE}
	    if [ ! -f "${CURRENTFILE}"  ]; then
 		echo "Missing file to baseline: ${CURRENTFILE}"
		#send mail
		exit 1
	    fi
	    FILENOEXT=$(basename "${CURRENTFILE}" .baseline)
	    echo baselinedata mv command: mv "${CURRENTFILE}" "${BASELINEDIR}/${NEWBASELINEDIR}/${FILENOEXT}"
	    mv "${CURRENTFILE}" "${BASELINEDIR}/${NEWBASELINEDIR}/${FILENOEXT}"
	done < "${INCREMENTALTABLESFILE}"
	rm ${BASELINEDIR}/current
	ln -s ${BASELINEDIR}/$NEWBASELINEDIR ${BASELINEDIR}/current
	mv "${OUTPUTDIR}"/*header* "${BASELINEDIR}/${NEWBASELINEDIR}"
}

dumpdata()
{
	#Make a copy of the files to be baselined - these are untouched/aka not mods
	while IFS= read -r t; do
		CURRENTFILE=$(ls ${FDIR}vam_${LOWERCASEVAM}_${t}_2*.txt)
		tail -n +2 "$CURRENTFILE" | sort -T ${OUTPUTDIR}  > "$CURRENTFILE".sorted
		mv "${CURRENTFILE}".sorted "${CURRENTFILE}.baseline"

		#Modify the file to put in the STATUS_FLAG
		sed -i -e '1s/$/|STATUS_FLAG/' -e '2,$s/$/|/' ${CURRENTFILE}
	done < "${INCREMENTALTABLESFILE}"
}

diffdata()
{
	#Validate that all of the files are present in the output dir
	while IFS= read -r t; do
		[ ! -f `ls ${FDIR}vam_${LOWERCASEVAM}_${t}_2*.txt` ] && doerror 1 "Diff failed:  No output file exists for config table: $t in ${OUTPUTDIR}"
	done < "${INCREMENTALTABLESFILE}"

	#Actually perform the diff
	while IFS= read -r t; do
		CURRENTFILE=`ls ${FDIR}vam_${LOWERCASEVAM}_${t}_2*.txt`
		BASELINEFILE=`ls ${BASELINEDIR}/current/*vam_${LOWERCASEVAM}_${t}_2*.txt`
		[ ! -f "$CURRENTFILE" ] && doerror 1 "The output file for table $t is missing" "${RECIPIENTS}"
		[ ! -f "$BASELINEFILE" ] && doerror 1 "A baseline file for table $t is missing" "${RECIPIENTS}" 
		HEADERROW=$(head -n 1 "$CURRENTFILE")
		#Store in baseline
		echo $HEADERROW > "$CURRENTFILE".baseline.header
		HEADERROW=$(echo "${HEADERROW}|STATUS_FLAG")
		#Remove header and make sure the files are sorted
		tail -n +2 "$CURRENTFILE" | sort -T ${OUTPUTDIR}  > "$CURRENTFILE".sorted 
		mv "$CURRENTFILE".sorted "$CURRENTFILE"

		#What position is the ID column - it should most be in the first position
		IDPOS=1
		if [[ ${CURRENTFILE} = *'CASE_DETAIL_ANSWER'* ]]; then
			IDPOS=2
		fi

		cut -d '|' -f ${IDPOS} ${CURRENTFILE}  > ${OUTPUTDIR}/current.ids.${t}.tmp
		cut -d '|' -f ${IDPOS} ${BASELINEFILE}  > ${OUTPUTDIR}/baseline.ids.${t}.tmp
		sort "${OUTPUTDIR}/current.ids.${t}.tmp" -o ${OUTPUTDIR}/current.ids.sorted.${t}.tmp
		rm -f ${OUTPUTDIR}/current.ids.${t}.tmp
		sort "${OUTPUTDIR}/baseline.ids.${t}.tmp" -o ${OUTPUTDIR}/baseline.ids.sorted.${t}.tmp
		rm -f ${OUTPUTDIR}/baseline.ids.${t}.tmp
       #comm args
       # -1     suppress lines unique to BASELINE files
       # -2     suppress lines unique in CURRENT file
       # -3     suppress lines that appear in BASELINE as well as CURRENT

		comm -1 -3 ${OUTPUTDIR}/baseline.ids.sorted.${t}.tmp ${OUTPUTDIR}/current.ids.sorted.${t}.tmp > ${OUTPUTDIR}/newids.${t}.tmp
		comm -2 -3 ${OUTPUTDIR}/baseline.ids.sorted.${t}.tmp ${OUTPUTDIR}/current.ids.sorted.${t}.tmp > ${OUTPUTDIR}/deletedids.${t}.tmp
		#Load the ids into memory
		declare -A newids_lookup	
		while IFS= read -r id; do
		    newids_lookup["$id"]=1
		done < ${OUTPUTDIR}/newids.${t}.tmp
		rm -f ${OUTPUTDIR}/newids.${t}.tmp	

		declare -A deletedids_lookup	
		while IFS= read -r id; do
		    deletedids_lookup["$id"]=1
		done < ${OUTPUTDIR}/deletedids.${t}.tmp
		rm -f ${OUTPUTDIR}/deletedids.${t}.tmp

		#Process NEW and UPDATED records
		comm -1 -3 ${BASELINEFILE} ${CURRENTFILE} > ${OUTPUTDIR}/neworupdated.${t}.tmp
		if [ -f ${OUTPUTDIR}/neworupdated.${t}.tmp ]; then
			while IFS= read -r u; do
				id=$(echo "$u" | cut -d '|' -f ${IDPOS})
				if [[ -n ${newids_lookup["$id"]} ]]; then
					echo "$u"  >> ${OUTPUTDIR}/newrecords.${t}.tmp
				else
					echo "$u"  >> ${OUTPUTDIR}/updatedrecords.${t}.tmp
				fi
			done < "${OUTPUTDIR}/neworupdated.${t}.tmp"
		fi

		#Process DELETED records - updated records are taken cared of above
		comm -2 -3 ${BASELINEFILE} ${CURRENTFILE} > ${OUTPUTDIR}/deletedorupdated.${t}.tmp
		if [ -f ${OUTPUTDIR}/deletedorupdated.${t}.tmp ]; then
			while IFS= read -r  v; do
				id=$(echo "$v" | cut -d '|' -f ${IDPOS})
				#grep -x -q "^${id}$" ${OUTPUTDIR}/updatedids.${t}.tmp
				if [[ -n ${deletedids_lookup["$id"]} ]]; then
					echo "$v"  >> ${OUTPUTDIR}/deletedrecords.${t}.tmp
				fi
			done < "${OUTPUTDIR}/deletedorupdated.${t}.tmp"
		fi

		#Combine files and put in the modified markers
	    	CURRENTINCREMENTALFILE=$(echo "${CURRENTFILE}" | sed "s/_"${t}"_2/_"${t}"_INCREMENTAL_2/g")
		echo $HEADERROW > $CURRENTINCREMENTALFILE
		if [ -f "${OUTPUTDIR}/newrecords.${t}.tmp" ]; then
			while IFS= read -r a; do
				echo "${a}|I" >> ${CURRENTINCREMENTALFILE}
			done < "${OUTPUTDIR}/newrecords.${t}.tmp"
		fi
		if [ -f "${OUTPUTDIR}/updatedrecords.${t}.tmp" ]; then
			while IFS= read -r b; do
				echo "${b}|U" >> ${CURRENTINCREMENTALFILE}
			done < "${OUTPUTDIR}/updatedrecords.${t}.tmp"
		fi
		if [ -f "${OUTPUTDIR}/deletedrecords.${t}.tmp" ]; then
			while IFS= read -r c; do
				echo "${c}|D" >> ${CURRENTINCREMENTALFILE}
			done < "${OUTPUTDIR}/deletedrecords.${t}.tmp"
		fi
		
		#Now that we have the incremental file, backup the original file  so it can be baselined
		mv "${CURRENTFILE}" "${CURRENTFILE}.baseline"
	done < "${INCREMENTALTABLESFILE}"
}
[ -z "${BASELINEDIR}" ] && doerror 1 "BASELINDIR export missing from parent script."

if [ -z "${CONFIGFILE}" ] || [ -z "${EXPORTSQLFILE}" ] || [ -z "${OUTPUTDIR}" ] || [ -z "${BATCHNUMBER}" ] || [ -z "${INCREMENTALTABLESFILE}" ] || [ -z "${TESTMODE}" ] || [ -z "${REVISIONMODE}" ]; then
	echo "Usage: $0 <config file> <batch number> <export SQL file> <incremental config file> <output dir> <test mode: Y/N> <revision mode: FULL/INCREMENTAL>"
	doerror 1 "(i.e.  $0  ../config/ibxcom_test/config.sh 456 ../config/ibxcom_test/exportSQL.txt ../config/ibxcom_test/incrementaltables.txt files N INCREMENTAL)"
fi

echo "dbExport_incremental using CONFIGFILE: ${CONFIGFILE}"
echo "dbExport_incremental using EXPORTSQLFILE: ${EXPORTSQLFILE}"
echo "dbExport_incremental using OUTPUTDIR: ${OUTPUTDIR}"
echo "dbExport_incremental using INCREMENTALTABLESFILE: ${INCREMENTALTABLESFILE}"
echo "dbExport_incremental using TESTMODE: ${TESTMODE}"
echo "dbExport_incremental using REVISIONMODE: ${REVISIONMODE}"
echo "dbExport_incremental using LIMITROWS: ${LIMITROWS}"

. $CONFIGFILE
LOWERCASEVAM=$(echo ${VAM[0]} | sed 's/./\L&/g')

SCRIPTNAME=`basename "$0"`

[ "${REVISIONMODE}" != "INCREMENTAL" ] && REVISIONMODE="FULL"

for i in ${!DBNAME[@]};
do

#Diff the data
[ "${REVISIONMODE}" == "INCREMENTAL" ] &&  diffdata

# No diff just create the baseline
[ "${REVISIONMODE}" == "FULL" ] && dumpdata

#Baseline the data
baselinedata

done
