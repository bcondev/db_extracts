#!/bin/bash

. $EXPORTHOME/deployment/scripts/common.sh


echo "Start : " ${DATETIMESTAMP}
echo ---------------------------------------------------------- 
echo 'Writing Files to :' ${OUTPUTDIR}

CONFIGFILE=$1
BATCHFILE=$2
CODEFILETABLE=$3
CODEFILETABLENOSTARTDATE=$4
OUTPUTDIR=$5
TESTMODE=$6

. $CONFIGFILE

get_batch_number ${BATCHFILE} ${TESTMODE}
echo "Batch Number : ${BATCHNUMBER}"
for i in ${!DBNAME[@]};
do
# Define Variables
DONEFILENAME=${FDIR}dwma2399_${DATESTAMP}_${TIMESTAMP}.done
FNAME=${FDIR}simplecodetables_${LOWERCASEVAM}_${DATESTAMP}
COUNT=0                             # record count for code file's .ctl file

if [ "${EXCLUDESIMPLECODEHEADER}" == "Y" ]; then
        DUMMY=1
else
        echo "SOURCE|SOURCE_AREA|CODE_SET_NAME|CODE_VALUE|CODE_DESC|EFFECTIVE_DT|TERMINATION_DT" >  ${FNAME}_${TIMESTAMP}.txt
fi

#dump tables that have id, description, start and end date
while read  t; do # start code files loop
if [ ! -z "${USENAMECODEFILE}" ] && [[ ${USENAMECODEFILE} == *'|'${t}'|'* ]]; then
	make_code_file ${t} "id" "SUBSTR(REPLACE(REPLACE(REPLACE(name,'\n',''),'\r',''),'|',' '),1,250)" "ifNull(start_dt,'0001-01-01'),ifNull(end_dt,'9999-12-31')"
else
	make_code_file ${t} "id" "SUBSTR(REPLACE(REPLACE(REPLACE(description,'\n',''),'\r',''),'|',' '),1,250)" "ifNull(start_dt,'0001-01-01'),ifNull(end_dt,'9999-12-31')"
fi
done < ${CODEFILETABLE}

#dump table that have id and description no start or end dates
while read  t; do # start code files loop
if [ ! -z "${USENAMECODEFILE}" ] && [[ ${USENAMECODEFILE} == *'|'${t}'|'* ]]; then
	make_code_file ${t} "id" "SUBSTR(REPLACE(REPLACE(REPLACE(name,'\n',''),'\r',''),'|',' '),1,250)" "'0001-01-01','9999-12-31'"
else
	make_code_file ${t} "id" "SUBSTR(REPLACE(REPLACE(REPLACE(description,'\n',''),'\r',''),'|',' '),1,250)" "'0001-01-01','9999-12-31'"
fi
done < ${CODEFILETABLENOSTARTDATE}

#(4) make control file
#
done

# Create done file, needs to be made after all jobs done
touch ${DONEFILENAME}
echo ---------------------------------------------------------- 
echo "Finish: " `date '+%m-%d-%Y %H:%M:%S'`
echo ----------------------------------------------------------
