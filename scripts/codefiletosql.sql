#!/bin/bash

LOGINPATH=$1
SCHEMA=$2
TABLENAME=$3

if [ -z "${LOGINPATH}" ] || [ -z "${SCHEMA}" ] || [ -z "${TABLENAME}" ];  then
        echo "Usage: $0 <schema> <table>"
        exit 1
fi
mysql --login-path=${LOGINPATH} --ssl-mode=DISABLED -sN ${SCHEMA} -B -e "select concat (upper('${TABLENAME}'), '~select ', group_concat(column_name), ' from ', upper('${TABLENAME}')) from information_schema.columns where lower(table_name) = lower('${TABLENAME}') and lower(table_schema) = lower('${SCHEMA}') order by ordinal_position asc;"
