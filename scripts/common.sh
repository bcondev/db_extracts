#!/bin/bash

doerror () {
        # param 1 is rc, 2 is error message
        echo "Usage: $0 <config name> <batch control dir> <Create consolidated CTL (Y/N)> [Mode (Y/N/FULL/INCREMENTAL)]"
        echo "${2}"
        exit ${1}
}

make_code_file(){
TABLE=$1
FIRSTLETTER=$(printf %.1s "${VAM[0]}")
echo 'Dumping Table : '${VAM[0]} $1
[ ! -z "${LIMITROWS}" ] && LIMITCLAUSE="LIMIT ${LIMITROWS}"
mysql --login-path=${LOGIN[$i]} --ssl-mode=DISABLED -sN ${DBNAME[$i]} -B -e "SELECT '${VAM[0]}','VAM','$TABLE',CONCAT('$FIRSTLETTER',$2),$3,$4 FROM $TABLE $LIMITCLAUSE;" | sed 's/|/ /g'  | sed 's/\\t//g' | sed 's/\\n//g' | sed 's/\t/|/g' | sed 's/–/-/g' | sed 's/≥/>=/g' | sed 's/≤/<=/g' | sed 's/•/*/g' | sed 's/…/.../g' | sed "s/’/'/g" | sed 's/ / /g' | sed 's/“/"/g' | sed 's/”/"/g' | sed "s/‘/'/g" | sed "s/’/'/g" | sed 's// /g' > ${FNAME}-DATA.txt
RESULT=`mysql --login-path=${LOGIN[$i]} --ssl-mode=DISABLED ${DBNAME[$i]} -B -e "SELECT count(*) FROM $TABLE;" | tr 'count(*)' ' '`
COUNT=`expr ${COUNT} + ${RESULT}`
cat ${FNAME}-DATA.txt >> ${FNAME}_${TIMESTAMP}.txt
rm ${FNAME}-DATA.txt
}

make_control_file()
{
	OUTPUTDIR=$1
	[ ! -d "${OUTPUTDIR}" ] && doerror 1 "Invalid output dir while making control file: ${OUTPUTDIR}"
	cd ${OUTPUTDIR}
	for i in `ls *.txt`; do
		ALLCOUNT=$(cat ${i} | wc -l)
		#don't count header row
		if [ "${EXCLUDESIMPLECODEHEADER}" == "Y" ] && [[ $i == *"simplecodetables"* ]]; then
        		DUMMY=1
		else
			ALLCOUNT=$((ALLCOUNT - 1))
		fi
		MODCOUNT=0
		NEWCOUNT=0
		DELCOUNT=0
		FILENOEXT="${i%.txt}"

		if [ "${REVISIONMODE}" == "INCREMENTAL" ]; then
			MODCOUNT=$(cat ${i} | grep '|U$' | wc -l)
			NEWCOUNT=$(cat ${i} | grep '|I$' | wc -l)
			DELCOUNT=$(cat ${i} | grep '|D$' | wc -l)
		fi

		OUTFILE=${FILENOEXT}.ctl
		[ "${GENCOMBINEDCTL}" == "Y" ] && OUTFILE=${FDIR}vam_${LOWERCASEVAM}_AllFiles_${DATESTAMP}_${TIMESTAMP}.ctl

		if [ ! -f "${OUTFILE}" ]; then
			if [ "${GENCOMBINEDCTL}" == "Y" ]; then
				if [ "${REVISIONMODE}" == "INCREMENTAL" ] || [ "${REVISIONMODE}" == "FULL" ]; then
                                	echo "FILE_NAME|SRC_BATCH_ID|SRC_SYS_ID|SRC_RCD_CNT|RCD_MOD_CNT|RCD_NEW_CNT|RCD_DEL_CNT|SRC_TOT_AMT1|SRC_TOT_AMT2|SRC_TOT_AMT3|EXT_DTTM|DATA_DT|FILE_FMT_VER_CD|SENDER_JOB_NM" > ${OUTFILE}
				else
                                	echo "FILE_NAME|SRC_BATCH_ID|SRC_SYS_ID|SRC_RCD_CNT|SRC_TOT_AMT1|SRC_TOT_AMT2|SRC_TOT_AMT3|EXT_DTTM|DATA_DT|FILE_FMT_VER_CD|SENDER_JOB_NM" > ${OUTFILE}
				fi
			else
                        	echo "SRC_BATCH_ID|SRC_SYS_ID|SRC_RCD_CNT|SRC_TOT_AMT1|SRC_TOT_AMT2|SRC_TOT_AMT3|EXT_DTTM|DATA_DT|FILE_FMT_VER_CD|SENDER_JOB_NM" > ${OUTFILE}
			fi
		fi

		#--- Create data row ---#
		if [ "${GENCOMBINEDCTL}" == "Y" ]; then
			if [ "${REVISIONMODE}" == "INCREMENTAL" ] || [ "${REVISIONMODE}" == "FULL" ]; then
				echo "${i}|$BATCHNUMBER|${VAM[0]}|$ALLCOUNT|$MODCOUNT|${NEWCOUNT}|${DELCOUNT}|0.00|0.00|0.00|$(date +'%Y%m%d %H:%M:%S')|$(date +'%Y%m%d')|1.0|`basename $0`" >> ${OUTFILE}
			else
				echo "${i}|$BATCHNUMBER|${VAM[0]}|$ALLCOUNT|0.00|0.00|0.00|$(date +'%Y%m%d %H:%M:%S')|$(date +'%Y%m%d')|1.0|`basename $0`" >> ${OUTFILE}
			fi
		else
			echo "$BATCHNUMBER|${VAM[0]}|$ALLCOUNT|0.00|0.00|0.00|$(date +'%Y%m%d %H:%M:%S')|$(date +'%Y%m%d')|1.0|`basename $0`" >> ${OUTFILE}
		fi
	done
}

get_batch_number()
{
	BATCHNUMBERFILE=$1
	TESTMODE=$2
	# handle control file batch number
	# if we don't have a file, start at zero
	if [ ! -f "${BATCHNUMBERFILE}" ] ; then
	  BATCHNUMBER=0

	# otherwise read the batchNumber from the file
	else
	  BATCHNUMBER=$(cat ${BATCHNUMBERFILE})
	fi

	# increment the batchNumber
	BATCHNUMBER=$(expr ${BATCHNUMBER} + 1)

	if [ "${TESTMODE}" == "N" ]; then
		echo "${BATCHNUMBER}" > ${BATCHNUMBERFILE}
	fi
}


dump_table_using_sql()
{
        TABLE=$1
        QUERYSQL=$2
        COUNT=0
        TESTMODE=$3
        #REVISIONMODE=$4
        #echo "Dumping Table from SQL: ${VAM[0]} $TABLE ${QUERYSQL}"
        echo "Dumping Table from SQL: $TABLE"
        LOWERCASEVAM=$(echo ${VAM[0]} | sed 's/./\L&/g')
        FNAME=$(echo "${FDIR}vam_${LOWERCASEVAM}_${TABLE}_${DATESTAMP}")
	#echo "Filename: ${FNAME}"
        PADDEDSQL="select wrapper.* from (select 1) as dummy left join ( ${QUERYSQL} ) as wrapper on true"
	[ ! -z "${LIMITROWS}" ] && PADDEDSQL=$(echo " ${PADDEDSQL=} LIMIT ${LIMITROWS}")
        mysql --login-path=${LOGIN[$i]} --ssl-mode=DISABLED ${DBNAME[$i]} --column-names -B -e "${PADDEDSQL}"  | sed 's/|/ /g'  | sed 's/\\t//g' | sed 's/\\n//g' | sed 's/\t/|/g' | sed -E 's/\b(NULL)\b//g' | sed 's/–/-/g' | sed 's/≥/>=/g' | sed 's/≤/<=/g' | sed 's/•/*/g' | sed 's/…/.../g' | sed "s/’/'/g" | sed 's/ / /g' | sed 's/“/"/g' | sed 's/”/"/g' | sed "s/‘/'/g" | sed "s/’/'/g" | sed 's/“/"/g' | sed 's/”/"/g' | sed "s/‘/'/g" | sed "s/’/'/g" | sed 's// /g' > ${FNAME}_${TIMESTAMP}.txt

        if [ $? != 0 ]; then
                echo "error SQL: ${PADDEDSQL}"
        fi
        #The count includes some sed processing so not to count the NULL rows
        COUNT=`cat ${FNAME}_${TIMESTAMP}.txt | sed 's/|//g' | sed '/^\s*$/d' | wc -l`
        #Dont include the header row in the number of count
        if [ ${COUNT} -gt 1 ]; then
                COUNT=`expr ${COUNT} - 1`
        else
                #Leave only the header row
                sed '1,1!d' ${FNAME}_${TIMESTAMP}.txt > ${FNAME}_${TIMESTAMP}.tmp
                mv ${FNAME}_${TIMESTAMP}.tmp ${FNAME}_${TIMESTAMP}.txt
                COUNT=0
        fi
        if [ "${TESTMODE}" == "Y" ]; then
                rm ${FNAME}_${TIMESTAMP}.*
        fi
}


get_batch_number()
{
	BATCHNUMBERFILE=$1
	MODE=$2
	# handle control file batch number
	# if we don't have a file, start at zero
	if [ ! -f "${BATCHNUMBERFILE}" ] ; then
	  BATCHNUMBER=0

	# otherwise read the batchNumber from the file
	else
	  BATCHNUMBER=`cat ${BATCHNUMBERFILE}`
	fi

	# increment the batchNumber
	BATCHNUMBER=`expr ${BATCHNUMBER} + 1`

	if [ "${TESTMODE}" != "Y" ]; then
		echo "${BATCHNUMBER}" > ${BATCHNUMBERFILE}
	fi
}


