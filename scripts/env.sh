#!/bin/bash

export DONEFILENAMES=(dwma2385 dwma2415 dwma2416 dwma2417 dwma2418 dwma2419 dwma2420 dwma2421)
[ -z "${DATESTAMP}" ] && export DATESTAMP=$(date +%Y%m%d)
[ -z "${TIMESTAMP}" ] && export TIMESTAMP=$(date +%H0000)
[ -z "${DATETIMESTAMP}" ] && export DATETIMESTAMP=`date '+%m-%d-%Y %H:%M:%S'`

cd ${SCRIPT_DIR}
cd ..
export EXPORTHOME=${PWD}
export BASELINEDIR=${EXPORTHOME}/baseline
export EXPORTLOG=${EXPORTHOME}/logs/${CONFIGNAME}
export FINALEXPORTOUTPUT=${EXPORTHOME}/files
export EXPORTOUTPUT=${EXPORTHOME}/wip
export CONFIGDIR=${EXPORTHOME}/deployment/config/${CONFIGNAME}

echo "Using EXPORTHOME: ${EXPORTHOME}"
echo "Using CONFIG DIR: ${CONFIGDIR}"
echo "Using BASELINE DIR: ${BASELINEDIR}"
echo "Using EXPORT LOG DIR: ${EXPORTLOG}"
echo "Using EXPORTOUTPUT WIP DIR: ${EXPORTOUTPUT}"
echo "Using FINAL OUTPUT DIR: ${FINALEXPORTOUTPUT}"


if [ ! -f "${CONFIGDIR}/config.sh"  ]; then
	echo "CONFIGFILE not found here: ${CONFIGDIR}/config.sh"
	exit 1
fi
. ${CONFIGDIR}/config.sh
if [ -z "${FPREFIX}"  ]; then
	echo "FILE Prefix not defined. i.e.  export FPREFIX=D_BEACONTEST_"
	exit 1
fi
export FDIR=${EXPORTOUTPUT}/${FPREFIX}
