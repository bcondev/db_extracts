#!/bin/bash

. $EXPORTHOME/deployment/scripts/common.sh

dump_incremental()
{
	TABLE=$1
	QUERYSQL=$2
	IDCOL=$3
	IDNAME=$4
	INCREMENTALCONFIGDATFILE=$5
	COUNT=0
	TESTMODE=$6

	if [ ! -z "${LIMITROWS}" ]; then
		QUERYSQL="${QUERYSQL} LIMIT ${LIMITROWS}"
	fi
	#echo 'Dumping incremental from SQL: '${VAM[0]} $TABLE"using query: "${QUERYSQL}
	echo "Dumping incremental from SQL: $TABLE"
	LOWERCASEVAM=$(echo ${VAM[0]} | sed 's/./\L&/g')
	FNAME=${FDIR}vam_${LOWERCASEVAM}_${TABLE}_${DATESTAMP}
	PADDEDSQL="select wrapper.* from (select 1) as dummy left join ( ${QUERYSQL} ) as wrapper on true"
	mysql --login-path=${LOGIN[$i]} --ssl-mode=DISABLED ${DBNAME[$i]} --column-names -B -e "${PADDEDSQL}"  | sed 's/|/ /g'  | sed 's/\\t//g' | sed 's/\\n//g' | sed 's/\t/|/g' | sed -E 's/\b(NULL)\b//g' | sed 's/–/-/g' | sed 's/≥/>=/g' | sed 's/≤/<=/g' | sed 's/•/*/g' | sed 's/…/.../g' | sed "s/’/'/g" | sed 's/ / /g' | sed 's/“/"/g' | sed 's/”/"/g' | sed "s/‘/'/g" | sed "s/’/'/g" | sed 's/“/"/g' | sed 's/”/"/g' | sed "s/‘/'/g" | sed "s/’/'/g" > ${FNAME}_${TIMESTAMP}.txt

	if [ $? != 0 ]; then
		echo "error SQL: ${PADDEDSQL}"
	fi
	#The count includes some sed processing so not to count the NULL rows
	COUNT=`cat ${FNAME}_${TIMESTAMP}.txt | sed 's/|//g' | sed '/^\s*$/d' | wc -l`
	#Dont include the header row in the number of count
	if [ ${COUNT} -gt 1 ]; then
		COUNT=`expr ${COUNT} - 1`
	else
		#Leave only the header row
		sed '1,1!d' ${FNAME}_${TIMESTAMP}.txt > ${FNAME}_${TIMESTAMP}.tmp
		mv ${FNAME}_${TIMESTAMP}.tmp ${FNAME}_${TIMESTAMP}.txt
		COUNT=0
	fi
	if [ "${TESTMODE}" == "Y" ]; then
		rm ${FNAME}_${TIMESTAMP}.*
	else
		#Update the incremental control file
		if [ ${COUNT} -gt 0 ]; then
			IDVALUE=`tail -1 ${FNAME}_${TIMESTAMP}.txt | cut -d'|' -f ${IDCOL}`
			if [ ! -z "${IDNAME}" ] && [ ! -z "${IDVALUE}" ] &&  [ -f "${INCREMENTALCONFIGDATFILE}" ]; then
				NOWTS=`date '+%Y-%m-%d %H:%M:%S'`
				echo "${IDNAME}^${IDVALUE}^EXECUTED_DATE^${NOWTS}" >> ${INCREMENTALCONFIGDATFILE}
			fi
		fi
	fi

}

CONFIGFILE=$1
BATCHNUMBER=$2
EXPORTSQLFILE=$3
OUTPUTDIR=$4
BATCHDIR=$5
TESTMODE=$6

if [ -z "${CONFIGFILE}" ] || [ -z "${EXPORTSQLFILE}" ] || [ -z "${OUTPUTDIR}" ] || [ -z "${BATCHNUMBER}" ] || [ -z "${BATCHDIR}" ]; then
	echo "Input files missing (i.e.  ./incrementalDBExport.sh ../config/ibxcom_test/config.sh 455 ../config/ibxcom_test/exportSQL.txt ../output/ibxcom_test ../../batch_control_data N)"
	exit 1
fi
if [ -z "${TESTMODE}" ]; then
	TESTMODE=Y
fi

. $CONFIGFILE

SCRIPTNAME=`basename "$0"`

if [ "${TESTMODE}" == "Y" ]; then
	echo 'Running in TEST mode... no files will be output.'
	mkdir __deleteme__
	FDIR=$PWD/__deleteme__/${FPREFIX}
fi

for i in ${!DBNAME[@]};
do

#dump incremental tables
while IFS= read -r t; do
	TABLENAME=`echo $t | cut -d'~' -f1`
	IDCOL=`echo $t | cut -d'~' -f2`
	IDNAME=`echo $t | cut -d'~' -f3`
	QUERYSQL=`echo $t | cut -d'~' -f4`
	if [ -n "${TABLENAME}" ] && [ -n "${QUERYSQL}" ]; then
		INCREMENTALCONFIGDATFILE=${BATCHDIR}/${TABLENAME}.incremental.dat
		if [ -f "${INCREMENTALCONFIGDATFILE}" ]; then
			ROWDATA=`tail -1 ${INCREMENTALCONFIGDATFILE}`
			IFS='~' read -r -a keyvalues <<< "${ROWDATA}"
			OLDIFS=${IFS}
			IFS=\\n
			for element in "${keyvalues[@]}"
			do
				EXPORTNAME=`echo $element | cut -d'^' -f1`
				EXPORTVALUE=`echo $element | cut -d'^' -f2`
				QUERYSQL=`echo ${QUERYSQL} | sed 's/{'${EXPORTNAME}'}/'${EXPORTVALUE}'/g'`
			done
			IFS=${OLDIFS}
			dump_incremental "${TABLENAME}" "${QUERYSQL}" "${IDCOL}" "${IDNAME}" "${INCREMENTALCONFIGDATFILE}" "${TESTMODE}"
		else
			echo "Missing incremental config file: ${INCREMENTALCONFIGDATFILE}"
			exit 1
		fi
	fi
done < "${EXPORTSQLFILE}"

done

if [ -d "./__deleteme__" ]; then
	rm -rf __deleteme__
fi
